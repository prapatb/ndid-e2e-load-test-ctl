const mkpath = require('mkpath');
const path = require('path');
const fs = require('fs');
const { REQ_STATE } = require('./constants');

const getRpApiAddress = nodeInfo =>
  `http${nodeInfo.isHttps ? 's' : ''}://${nodeInfo.domain}:${nodeInfo.port}`;

const getJobJson = job => ({
  job_id: job.job_id,
  duration: job.duration,
  interval: job.interval,
  requestsPerInterval: job.requestsPerInterval,
  roundRobin: job.roundRobin,
  tps: job.tps,
  total_requests: job.total_requests,
  forceStopped: job.forceStopped,
  stopped: job.stopped,
  cancelled: job.cancelled,
  finished: job.finished,
  summary: job.summary,
  timestamps: job.timestamps,
  timeSummary: job.timeSummary,
  archivedRequests: job.archivedRequests,
  idpList: job.idpList,
  asList: job.asList,
  rpAddressList: job.rpAddressList
});

const archiveJobRequests = (requests, jobId, archiveDirPath) => {
  const newReqs = {};
  const reqsToArchive = {};
  Object.keys(requests).forEach(refId => {
    if (requests[refId].job_id === jobId) {
      reqsToArchive[refId] = requests[refId];
    } else {
      newReqs[refId] = requests[refId];
    }
  });

  mkpath.sync(archiveDirPath);

  const filePath = path.join(archiveDirPath, `${jobId}.json`);
  fs.writeFile(filePath, JSON.stringify(reqsToArchive, null, 2), err => {
    if (err) {
      return console.log(`[Job ID: ${jobId}] Archiving job data failed`, err);
    }

    console.log(`[Job ID: ${jobId}] Archived job data to ${filePath}`);
  });

  return newReqs;
};

const getPriorReqState = state => {
  switch (state) {
    case REQ_STATE.CREATED:
      return REQ_STATE.REQUESTED;
    case REQ_STATE.PENDING:
      return REQ_STATE.CREATED;
    case REQ_STATE.CONFIRMED:
      return REQ_STATE.PENDING;
    case REQ_STATE.DATA_SIGNED:
      return REQ_STATE.CONFIRMED;
    case REQ_STATE.COMPLETED:
      return REQ_STATE.DATA_SIGNED;
    case REQ_STATE.CLOSED:
      return REQ_STATE.COMPLETED;

    default:
      break;
  }

  return null;
};

module.exports = {
  getRpApiAddress,
  getJobJson,
  archiveJobRequests,
  getPriorReqState,
};
