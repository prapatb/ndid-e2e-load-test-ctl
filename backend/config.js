const path = require("path")

const config = {  
  HTTP_PORT: process.env.CTL_PORT || 2201,
  STATIC_FOLDER: path.join(__dirname, 'public'),
  CALLBACK_URL_RP: `http://${process.env.CTL_IP || 'e2e-load-test-ctl'}:${process.env.CTL_PORT || 2201}/rp/request`,
  RP_API_ADDRESS: 'http://api-rp:8080',
  RP_API_NODES: [
    {
      isHttps: false,
      domain: 'api-rp-1',
      port: '8080'
    },
    {
      isHttps: false,
      domain: 'api-rp-2',
      port: '8080'
    }
  ],
  LOG_LEVEL: process.env.LOG_LEVEL || 'error',
  ARCHIVE_FOLDER: process.env.ARCHIVE_FOLDER || path.join(__dirname, 'archive'),
}

module.exports = config
