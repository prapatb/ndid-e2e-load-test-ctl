const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const cors = require('cors');
const requestPromise = require('request-promise');
const uuid = require('uuid/v4');
const socketIO = require('socket.io');
const _ = require('lodash');
const accurateInterval = require('accurate-interval');
const config = require('./config');
const utility = require('./utility');
const { REQ_STATE } = require('./constants')


const mockServs = {
  rp: [],
  idp: [],
  as: []
}

let requests = {};
const jobs = {};

const app = express();
app.use(cors()) // Allow CORS
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

const logRequestStart = (req, res, next) => {
  console.info(`${req.method} ${req.originalUrl}: \n${JSON.stringify(req.body)}`);
  next();
}
if (config.LOG_LEVEL === 'info' || config.LOG_LEVEL === 'debug') {
  app.use(logRequestStart);
}

app.post('/api/registerNode', (req, res) => {
  const {
    node_id,
    ip,
    port,
    role
  } = req.body;
  if (!node_id || !ip || (role !== 'rp' && role !== 'idp' && role !== 'as')) {
    res.status(400).end();
    return;
  }

  const existingNode = mockServs[role].find(node => node.node_id === node_id);
  if (existingNode) {
    existingNode.ip = ip;
    existingNode.port = port;

  } else {
    mockServs[role].push({
      node_id,
      ip,
      port: port || 80,
      role
    })
  }

  res.status(existingNode ? 204 : 201).end();
});

app.post('/api/deleteNode/:role/:node_id', (req, res) => {
  const {
    role,
    node_id
  } = req.params;
  if (!role || !node_id) {
    res.status(400).end();
    return;
  }

  const idx = mockServs[role] && mockServs[role].findIndex(node => node.node_id === node_id);
  if (idx !== -1) {
    mockServs[role].splice(idx, 1);
  }

  res.status(204).end();
});

const fireRequests = (jobId, namespace, identifier, serviceId, idpList = [], asList = [], rpAddresses = [], totalReqs, timeout) => {
  const idpIdList = idpList.length ? idpList : mockServs.idp.map(node => node.node_id);
  const asIdList = asList.length ? asList : mockServs.as.map(node => node.node_id);

  const min_ial = 2.3;
  const min_aal = 2.2;
  const req_msg = JSON.stringify({ id: jobId, count: totalReqs });
    
  rpAddresses.forEach(rpAddr => {
    const refId = uuid();
    const body = {
      'reference_id': refId,
      'mode': 1,
      'idp_id_list': idpIdList,
      'data_request_list': [{
        'service_id': serviceId,
        'as_id_list': asIdList,
        'min_as': asIdList.length,
        'request_params': req_msg,
      }],
      'request_message': req_msg,
      'min_idp': idpIdList.length,
      'min_ial': min_ial,
      'min_aal': min_aal,
      'request_timeout': timeout,
      'callback_url': `${config.CALLBACK_URL_RP}/${refId}`
    };

    // console.log(`Sending create request to API: ${rpAddr}/rp/requests/${namespace}/${identifier}\n`, JSON.stringify(body, null, 2));
    requests[refId] = {
      reference_id: refId,
      job_id: jobId,
      node_id: 'ctl',
      state: REQ_STATE.REQUESTED,
      timestamps: {
        [REQ_STATE.REQUESTED]: new Date()
      },
      rp_addr: rpAddr,
    };    
    requestPromise({
      method: 'POST',
      uri: `${rpAddr}/rp/requests/${namespace}/${identifier}`,
      body: body,
      json: true,
      resolveWithFullResponse: true,
      simple: false,
      rejectUnauthorized: false // for self signed https
    })
    .then(response => {
      if (!response.body.request_id) {
        throw new Error((response.body.error && JSON.stringify(response.body.error, null, 2)) || 'Unknown error');
      }

      requests[refId].request_id = response.body.request_id;
      console.log(`[Job ID: ${jobId}] Request created - RP: ${rpAddr}, Ref. ID: ${refId}, Req. ID: ${response.body.request_id}`);
    })
    .catch(error => {
      console.error(`[Job ID: ${jobId}] Create request error - RP: ${rpAddr}, Ref. ID: ${refId}\nRequest body: ${JSON.stringify(body, null, 2)}\n${error}`);
    })
    console.log(`[Job ID: ${jobId}] Requested to RP API - RP: ${rpAddr}, Ref. ID: ${refId}`);
  });
}

const closeRequest = (request) => {
  const {
    reference_id: refId,
    request_id: reqId,
    rp_addr: rpAddr,
    job_id: jobId
  } = request;

  const body = {
    reference_id: refId,
    callback_url: `${config.CALLBACK_URL_RP}/${refId}`,
    request_id: reqId,
  };

  requestPromise({
    method: 'POST',
    uri: `${rpAddr}/rp/requests/close`,
    body: body,
    json: true,
    resolveWithFullResponse: true,
    simple: false,
    rejectUnauthorized: false // for self signed https
  })
  .then(() => {
    console.log(`[Job ID: ${jobId}] Close request submitted - RP: ${rpAddr}, Ref. ID: ${refId}, Req. ID: ${reqId}`);
  })
  .catch(error => {
    console.error(`[Job ID: ${jobId}] Close request error - RP: ${rpAddr}, Ref. ID: ${refId}, Req. ID: ${reqId}\nRequest body: ${JSON.stringify(body, null, 2)}\n${error}`);
  });
}

const calculateJobSummary = (jobId) => {
  const job = jobs[jobId];
  if (!job) {
    return {};
  }

  const jobReqs = Object.values(requests).filter(r => r.job_id === jobId);

  return {
    [REQ_STATE.REQUESTED]: jobReqs.filter(r => r.state === REQ_STATE.REQUESTED).length,
    [REQ_STATE.CREATED]: jobReqs.filter(r => r.state === REQ_STATE.CREATED).length,
    [REQ_STATE.PENDING]: jobReqs.filter(r => r.state === REQ_STATE.PENDING).length,
    [REQ_STATE.CONFIRMED]: jobReqs.filter(r => r.state === REQ_STATE.CONFIRMED).length,
    [REQ_STATE.DATA_SIGNED]: jobReqs.filter(r => r.state === REQ_STATE.DATA_SIGNED).length,
    [REQ_STATE.COMPLETED]: jobReqs.filter(r => r.state === REQ_STATE.COMPLETED).length,
    [REQ_STATE.CLOSED]: jobReqs.filter(r => r.state === REQ_STATE.CLOSED).length,
    [REQ_STATE.CREATE_FAILED]: jobReqs.filter(r => r.state === REQ_STATE.CREATE_FAILED).length,
    [REQ_STATE.TIMED_OUT]: jobReqs.filter(r => r.state === REQ_STATE.TIMED_OUT).length,
    [REQ_STATE.UNKNOWN]: jobReqs.filter(
      r => r.state !== REQ_STATE.REQUESTED
        && r.state !== REQ_STATE.CREATED
        && r.state !== REQ_STATE.PENDING
        && r.state !== REQ_STATE.CONFIRMED
        && r.state !== REQ_STATE.DATA_SIGNED
        && r.state !== REQ_STATE.COMPLETED
        && r.state !== REQ_STATE.CLOSED
        && r.state !== REQ_STATE.CREATE_FAILED
        && r.state !== REQ_STATE.TIMED_OUT
    ).length,
  };
}

app.post('/api/createRequests', async (req, res) => {
  const { 
    jobId: inputJobId,
    namespace,
    identifier,
    serviceId,
    rpAddressList = [],
    interval = 0, 
    duration = 0, 
    requestsPerInterval = 1, 
    roundRobin = true,
    idpList = [], 
    asList = [],
    timeout = 3600
  } = req.body;

  if (!namespace || !identifier || !serviceId) {
    const requireList = [];
    if (!namespace) {
      requireList.push('namespace');
    }
    if (!identifier) {
      requireList.push('identifier');
    }
    if (!serviceId) {
      requireList.push('serviceId');
    }
    res.status(400).json({
      message: `${requireList.join(', ')} must not be empty.`
    });
    return;
  }

  if (interval <= 0 || duration <= 0) {
    res.status(400).json({
      message: 'Interval and duration must be greater than 0'
    });
    return;
  }

  if (interval > duration) {
    res.status(400).json({
      message: 'Duration must be equal or greater than interval'
    });
    return;
  }

  if (inputJobId && jobs[inputJobId]) {
    res.status(400).json({
      message: 'Job ID already exists'
    });
    return;
  }

  const jobId = inputJobId || uuid();  
  const useRpInConfigFile = !rpAddressList || rpAddressList.length === 0;
  const rpAddrs = (useRpInConfigFile ? config.RP_API_NODES.map(utility.getRpApiAddress) : rpAddressList) || [];
  let intervalIdx = 1;
  let roundRobinIdx = 0;
  const numberOfIntervals = Math.floor(duration / interval);
  const totalReqs = numberOfIntervals * requestsPerInterval * (roundRobin ? 1 : rpAddrs.length);
  const tps = requestsPerInterval / interval * (roundRobin ? 1 : rpAddrs.length);
  console.log(`[Job ID: ${jobId}] Going to fire ${numberOfIntervals} interval(s), ${requestsPerInterval} request(s) / interval / RP, to ${rpAddrs.length} RP(s)${roundRobin ? ' with round robin' : ''}, total ${totalReqs} request(s)`);

  const timer = accurateInterval(() => {
    if (intervalIdx > numberOfIntervals) {
      stopJob(jobId);
      return;
    }
    
    console.log(`[Job ID: ${jobId}] Fire request interval no.: ${intervalIdx} at ${Date.now()}`);
    
    for (let i = 0; i < requestsPerInterval; i++) {
      let rpAddrsToCall = rpAddrs;
      if (roundRobin) {
        const toCallIndex = roundRobinIdx % rpAddrs.length;
        rpAddrsToCall = rpAddrs.slice(toCallIndex, toCallIndex + 1);
      }
      fireRequests(jobId, namespace, identifier, serviceId, idpList, asList, rpAddrsToCall, totalReqs, timeout);
      roundRobinIdx++;
    }

    intervalIdx++;
    
  }, interval * 1000, { aligned: true })

  jobs[jobId] = { 
    job_id: jobId,
    duration,
    interval,
    requestsPerInterval,
    roundRobin,
    idpList,
    asList,
    rpAddressList,
    tps,
    total_requests: totalReqs,
    timer,
    stopped: false, 
    finished: false, 
    timestamps: { started: new Date() },
    timeSummary: {
      diff: {},
      state: {},
    },
    archivedRequests: false,
  };

  const updateSummaryIntervalId = setInterval(() => {
    const toUpdateJob = jobs[jobId];
    
    if (toUpdateJob.cancelled) {
      if (toUpdateJob.toForceCloseRequestCount <= toUpdateJob.forceClosedRequestCount) {
        toUpdateJob.cancelCompleted = true;
        console.log(`[Job ID: ${jobId}] Closed all ${toUpdateJob.toForceCloseRequestCount} remaining request(s)`);
      }
    }

    const summary = calculateJobSummary(jobId);
    toUpdateJob.summary = summary;

    if (!toUpdateJob.finished) {
      toUpdateJob.finished = toUpdateJob.stopped 
        && summary[REQ_STATE.REQUESTED] === 0
        && summary[REQ_STATE.CREATED] === 0
        && summary[REQ_STATE.PENDING] === 0
        && summary[REQ_STATE.DATA_SIGNED] === 0
        && summary[REQ_STATE.CONFIRMED] === 0
        && summary[REQ_STATE.COMPLETED] === 0;

      if (toUpdateJob.finished) {
        toUpdateJob.timestamps.finished = new Date();
        console.log(`[Job ID: ${jobId}] Job finished`) 
      }
    }    
      
    const json = Object.assign({}, toUpdateJob);
    // Remove self-ref properties
    delete json.timer;
    delete json.update_summary_interval_id;
    
    io.sockets.emit('job:update', json);

    if (toUpdateJob.finished || toUpdateJob.cancelCompleted) {
      clearInterval(updateSummaryIntervalId);
      jobs[jobId].update_summary_interval_id = null;
      requests = utility.archiveJobRequests(requests, jobId, config.ARCHIVE_FOLDER);
      jobs[jobId].archivedRequests = true;
    }
  }, 1000);
  jobs[jobId].update_summary_interval_id = updateSummaryIntervalId;

  res.status(202).json({ job_id: jobId });
});

const stopJob = (jobId) => {
  jobs[jobId].stopped = true;
  jobs[jobId].timestamps.stopped = new Date();

  console.log(`[Job ID: ${jobId}] Job stopped`);
  jobs[jobId].timer.clear();
  jobs[jobId].timer = null;
}

app.post('/api/stopJob/:job_id', (req, res) => {
  const jobId = req.params.job_id;
  const job = jobs[jobId];

  if (!job) {
    res.status(400).json('No job ID found.');
  }

  if (job.stopped || job.forceStopped) {
    res.status(400).json('Job already stopped.');
  }

  job.forceStopped = true;
  stopJob(jobId);
  
  res.status(204).end();
})

app.post('/api/cancelJob/:job_id', (req, res) => {
  const jobId = req.params.job_id;
  const job = jobs[jobId];

  if (!job) {
    res.status(400).json('No job ID found.');
  }

  if (job.finished) {
    res.status(400).json('Job already finished.');
  }

  if (job.cancelled) {
    res.status(400).json('Job already cancelled.');
  }

  job.cancelled = true;
  job.timestamps.cancelled = new Date();

  console.log(`[Job ID: ${jobId}] Job cancelled`);
  if (job.timer) {
    job.timer.clear();
    job.timer = null;
  }

  const remainingReqs = Object
    .values(requests)
    .filter(r => 
      r.job_id === jobId && r.state !== REQ_STATE.CLOSED && r.state !== REQ_STATE.TIMED_OUT && r.state !== REQ_STATE.CREATE_FAILED)        
    
  job.toForceCloseRequestCount = remainingReqs.length;
  job.forceClosedRequestCount = 0;
  remainingReqs.forEach(r => {
    r.forceCloseRequested = true;
    closeRequest(r);
  });
  
  res.status(204).end();
})

const determineTimestampStateName = (newReqStatus) => {  
  if (newReqStatus.type === 'create_request_result') {
    if (newReqStatus.success) {
      return REQ_STATE.CREATED;
    } else {
      return REQ_STATE.CREATE_FAILED;
    }
  }

  if (newReqStatus.type === 'close_request_result') {
    if (newReqStatus.success || (newReqStatus.error && newReqStatus.error.code === 25002)) {
      return REQ_STATE.CLOSED;
    }
    return null;
  }

  if (newReqStatus.closed) {
    return REQ_STATE.CLOSED;
  }
  if (newReqStatus.timed_out) {
    return REQ_STATE.TIMED_OUT;
  }
  if (newReqStatus.status === 'pending') {
    return REQ_STATE.PENDING;
  }
  if (newReqStatus.status === 'confirmed') {
    if (newReqStatus.service_list.filter(dataReq => dataReq.signed_data_count < dataReq.min_as).length === 0) {
      return REQ_STATE.DATA_SIGNED;
    }

    return REQ_STATE.CONFIRMED;
  }
  if (newReqStatus.status === 'completed') {
    return REQ_STATE.COMPLETED;
  }
}


const updateRequestStatus = (request_status) => {
  const request = requests[request_status.reference_id];
  if (!request) {
    // Case: The request might not be sent by this controller
    //       or already archived
    return;
  }

  const tsStateName = determineTimestampStateName(request_status);

  try {
    if (tsStateName) {
      if (request.state !== REQ_STATE.CLOSED && request.state !== REQ_STATE.TIMED_OUT && request.state !== REQ_STATE.CREATE_FAILED) {
        if (request.forceCloseRequested) {          
          if (tsStateName === REQ_STATE.CLOSED && !request.forceClosed) {
            request.forceClosed = true;
            jobs[request.job_id].forceClosedRequestCount += 1;
          }

        } else {
          request.state = tsStateName;
          if (!request.timestamps[tsStateName]) {
            request.timestamps[tsStateName] = new Date();
            updateJobTimeSummaryWithNewRequestState(request.job_id, request_status.reference_id, tsStateName);
          }
        }
      }      
    } else {
      console.log(
        `[Job ID: ${request.job_id}] Cannot determine timestamp state name.\nNew status: ${JSON.stringify(request_status, null, 2)}`
      );
    }

  } catch(e) {
    console.log(`[Job ID: ${request.job_id}] Failed to update request status - Ref. ID: ${request_status.reference_id}, Req. ID: ${request.request_id}\n${e}`);
  }
}


app.get('/api/nodes', (req, res) => {
  res.status(200).json(mockServs);
});

app.post('/api/nodes', (req, res) => {
  const { rp, idp, as } = req.body;
  mockServs.rp = rp || [];
  mockServs.idp = idp || [];
  mockServs.as = as || [];

  res.status(204).end();
});

app.get('/api/requests', (req, res) => {
  res.status(200).json(requests);
});

app.post('/api/nodes/clearUnavailable', (req, res) => {
  res.status(501).end();
});

app.get('/api/jobs', (req, res) => {
  const result = Object
    .keys(jobs)
    .reduce((prev, jobId) => {
      const job = jobs[jobId];
      prev[jobId] = utility.getJobJson(job);
      return prev;
    }, {});
  res.status(200).json(result);
})

const updateJobTimeSummaryDiffWithNewRequestState = (job, req, newReqState) => {
  const priorReqState = utility.getPriorReqState(newReqState);

  if (!priorReqState) {
    return;
  }

  const diff = req.timestamps[newReqState] - req.timestamps[priorReqState];
  if (!job.timeSummary.diff[newReqState]) {
    job.timeSummary.diff[newReqState] = {
      requestCount: 1,
      min: diff,
      avg: diff,
      max: diff
    };
    return;
  }

  const diffTimeSummary = job.timeSummary.diff[newReqState];
  const newReqCount = diffTimeSummary.requestCount + 1;
  job.timeSummary.diff[newReqState] = {
    requestCount: newReqCount,
    min: diffTimeSummary.min > diff ? diff : diffTimeSummary.min,
    avg: (diffTimeSummary.avg * diffTimeSummary.requestCount + diff) / newReqCount,
    max: diffTimeSummary.max < diff ? diff : diffTimeSummary.max
  };
}

const updateJobTimeSummaryStateWithNewRequestState = (job, req, newReqState) => {
  const reqStateDuration = req.timestamps[newReqState] - req.timestamps[REQ_STATE.REQUESTED]
  if (!job.timeSummary.state[newReqState]) {
    job.timeSummary.state[newReqState] = {
      requestCount: 1,
      min: reqStateDuration,
      avg: reqStateDuration,
      max: reqStateDuration
    };
    return;
  }

  const stateTimeSummary = job.timeSummary.state[newReqState];
  const newReqCount = stateTimeSummary.requestCount + 1;
  job.timeSummary.state[newReqState] = {
    requestCount: newReqCount,
    min: stateTimeSummary.min > reqStateDuration ? reqStateDuration : stateTimeSummary.min,
    avg: (stateTimeSummary.avg * stateTimeSummary.requestCount + reqStateDuration) / newReqCount,
    max: stateTimeSummary.max < reqStateDuration ? reqStateDuration : stateTimeSummary.max
  };
}

const updateJobTimeSummaryWithNewRequestState = (jobId, refId, newReqState) => {
  const job = jobs[jobId];
  const req = requests[refId];

  updateJobTimeSummaryDiffWithNewRequestState(job, req, newReqState);  
  updateJobTimeSummaryStateWithNewRequestState(job, req, newReqState);
}

app.get('/api/timeSummary/:job_id', (req, res) => {
  const { job_id } = req.params;
  const job = jobs[job_id];

  if (!job) {
    res.status(404).end();
    return;
  }

  const elapsedTime = job.timestamps.finished ? 
    job.timestamps.finished - job.timestamps.started :
    new Date() - job.timestamps.started;
  res.send(
    job_id + ',' +
    job.interval + ',' + 
    job.duration + ',' + 
    job.requestsPerInterval + ',' +
    job.tps + ',' +
    job.total_requests + ',' + 
    job.finished + ',' +
    elapsedTime + ',' +
    (job.timeSummary.state[REQ_STATE.CREATED] ? job.timeSummary.state[REQ_STATE.CREATED].requestCount : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.PENDING] ? job.timeSummary.state[REQ_STATE.PENDING].requestCount : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.CONFIRMED] ? job.timeSummary.state[REQ_STATE.CONFIRMED].requestCount : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.DATA_SIGNED] ? job.timeSummary.state[REQ_STATE.DATA_SIGNED].requestCount : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.COMPLETED] ? job.timeSummary.state[REQ_STATE.COMPLETED].requestCount : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.CLOSED] ? job.timeSummary.state[REQ_STATE.CLOSED].requestCount : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.CREATED] ? _.round(job.timeSummary.state[REQ_STATE.CREATED].min, 2) : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.CREATED] ? _.round(job.timeSummary.state[REQ_STATE.CREATED].avg, 2) : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.CREATED] ? _.round(job.timeSummary.state[REQ_STATE.CREATED].max, 2) : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.PENDING] ? _.round(job.timeSummary.state[REQ_STATE.PENDING].min, 2) : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.PENDING] ? _.round(job.timeSummary.state[REQ_STATE.PENDING].avg, 2) : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.PENDING] ? _.round(job.timeSummary.state[REQ_STATE.PENDING].max, 2) : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.CONFIRMED] ? _.round(job.timeSummary.state[REQ_STATE.CONFIRMED].min, 2) : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.CONFIRMED] ? _.round(job.timeSummary.state[REQ_STATE.CONFIRMED].avg, 2) : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.CONFIRMED] ? _.round(job.timeSummary.state[REQ_STATE.CONFIRMED].max, 2) : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.DATA_SIGNED] ? _.round(job.timeSummary.state[REQ_STATE.DATA_SIGNED].min, 2) : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.DATA_SIGNED] ? _.round(job.timeSummary.state[REQ_STATE.DATA_SIGNED].avg, 2) : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.DATA_SIGNED] ? _.round(job.timeSummary.state[REQ_STATE.DATA_SIGNED].max, 2) : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.COMPLETED] ? _.round(job.timeSummary.state[REQ_STATE.COMPLETED].min, 2) : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.COMPLETED] ? _.round(job.timeSummary.state[REQ_STATE.COMPLETED].avg, 2) : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.COMPLETED] ? _.round(job.timeSummary.state[REQ_STATE.COMPLETED].max, 2) : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.CLOSED] ? _.round(job.timeSummary.state[REQ_STATE.CLOSED].min, 2) : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.CLOSED] ? _.round(job.timeSummary.state[REQ_STATE.CLOSED].avg, 2) : 0) + ',' +
    (job.timeSummary.state[REQ_STATE.CLOSED] ? _.round(job.timeSummary.state[REQ_STATE.CLOSED].max, 2) : 0) + ',' +    
    (job.timeSummary.diff[REQ_STATE.PENDING] ? _.round(job.timeSummary.diff[REQ_STATE.PENDING].min, 2) : 0) + ',' +
    (job.timeSummary.diff[REQ_STATE.PENDING] ? _.round(job.timeSummary.diff[REQ_STATE.PENDING].avg, 2) : 0) + ',' +
    (job.timeSummary.diff[REQ_STATE.PENDING] ? _.round(job.timeSummary.diff[REQ_STATE.PENDING].max, 2) : 0) + ',' +
    (job.timeSummary.diff[REQ_STATE.CONFIRMED] ? _.round(job.timeSummary.diff[REQ_STATE.CONFIRMED].min, 2) : 0) + ',' +
    (job.timeSummary.diff[REQ_STATE.CONFIRMED] ? _.round(job.timeSummary.diff[REQ_STATE.CONFIRMED].avg, 2) : 0) + ',' +
    (job.timeSummary.diff[REQ_STATE.CONFIRMED] ? _.round(job.timeSummary.diff[REQ_STATE.CONFIRMED].max, 2) : 0) + ',' +
    (job.timeSummary.diff[REQ_STATE.DATA_SIGNED] ? _.round(job.timeSummary.diff[REQ_STATE.DATA_SIGNED].min, 2) : 0) + ',' +
    (job.timeSummary.diff[REQ_STATE.DATA_SIGNED] ? _.round(job.timeSummary.diff[REQ_STATE.DATA_SIGNED].avg, 2) : 0) + ',' +
    (job.timeSummary.diff[REQ_STATE.DATA_SIGNED] ? _.round(job.timeSummary.diff[REQ_STATE.DATA_SIGNED].max, 2) : 0) + ',' +
    (job.timeSummary.diff[REQ_STATE.COMPLETED] ? _.round(job.timeSummary.diff[REQ_STATE.COMPLETED].min, 2) : 0) + ',' +
    (job.timeSummary.diff[REQ_STATE.COMPLETED] ? _.round(job.timeSummary.diff[REQ_STATE.COMPLETED].avg, 2) : 0) + ',' +
    (job.timeSummary.diff[REQ_STATE.COMPLETED] ? _.round(job.timeSummary.diff[REQ_STATE.COMPLETED].max, 2) : 0) + ',' +
    (job.timeSummary.diff[REQ_STATE.CLOSED] ? _.round(job.timeSummary.diff[REQ_STATE.CLOSED].min, 2) : 0) + ',' +
    (job.timeSummary.diff[REQ_STATE.CLOSED] ? _.round(job.timeSummary.diff[REQ_STATE.CLOSED].avg, 2) : 0) + ',' +
    (job.timeSummary.diff[REQ_STATE.CLOSED] ? _.round(job.timeSummary.diff[REQ_STATE.CLOSED].max, 2) : 0)
  ).end();
})

app.get('/api/jobTimeSummary/:job_id', (req, res) => {
  const { job_id } = req.params;
  const job = jobs[job_id];
  if (!job) {
    res.status(404).end();
    return;
  }

  if (!job.finished) {
    res.status(400).json('Job does not finished yet.');
    return;
  }  

  res.status(200).json({
    ...job.timeSummary
  });
})

// ################################################
// RP Callback
// ################################################

app.post("/rp/request/:referenceId", (req, res) => {
  // console.log('Recieved request callback: ', JSON.stringify(req.body, null, 2));

  let requestStatus = { ...req.body, reference_id: req.params.referenceId };
  updateRequestStatus(requestStatus);

  res.status(200).end();
});

// Serve static files
app.use('/', (req, res) => {
  express.static(config.STATIC_FOLDER, {
    lastModified: true,
    maxAge: '1d'
  })(req, res, () => {
    res.sendFile(config.STATIC_FOLDER + '/index.html');
  });
});

const allMockServsCount = () => {
  return mockServs.rp.length + mockServs.idp.length + mockServs.as.length;
}

// Interval check node status
const checkNodeStatusIntervalId = setInterval(async () => {
  Object.values(mockServs).forEach(servs => {
    servs.forEach(async aServ => {
      try {
        await requestPromise({
          method: 'POST',
          uri: `http://${aServ.ip}:${aServ.port}/alive`,
          json: true,
          resolveWithFullResponse: true,
          simple: false
        });
        aServ.status = 'online';
  
      } catch (error) {
        aServ.status = 'offline';
      }
    })
  })
  
}, 1000);

const server = http.createServer(app)

const io = socketIO(server, { path: '/ws' });
io.on('connection', socket => {
  console.log('Client connected');
  
  socket.on('disconnect', () => {
    console.log('Client disconnected');
  })
})

server.listen(config.HTTP_PORT, () => {
  console.log('Listening on port ' + config.HTTP_PORT);  
})