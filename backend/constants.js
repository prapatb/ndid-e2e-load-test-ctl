const REQ_STATE = {
  REQUESTED: 'requested',
  CREATED: 'created',
  PENDING: 'pending',
  CONFIRMED: 'confirmed',
  DATA_SIGNED: 'data_signed',
  COMPLETED: 'completed',
  CLOSED: 'closed',
  CREATE_FAILED: 'create_failed',
  TIMED_OUT: 'timed_out',
  UNKNOWN: 'unknown'
}

module.exports = {
  REQ_STATE
}
