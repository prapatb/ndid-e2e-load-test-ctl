import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import './App.css';
import ControlPanelContainer from './containers/ControlPanelContainer'
import StatusPanelContainer from './containers/StatusPanelContainer'
import { initialized } from './actions';


const StyledPanelTitle = styled.h2`
  font-size: 1.25rem;
`

const StyledPanelWrapper = styled.div`
  margin: 16px 20px;
  padding: 16px 10px;
  border: 1px solid #bbb;
`

const mapDispatchToProps = dispatch => ({
  onIntialized: () => {
    dispatch(initialized());
  }
})

class App extends Component {
  componentDidMount() {
    this.props.onIntialized();
  }

  render() {
    return (
      <div className="App">        
        <StyledPanelWrapper>
          <StyledPanelTitle>CONTROL</StyledPanelTitle>
          <hr />
          <ControlPanelContainer />
        </StyledPanelWrapper>
        <StyledPanelWrapper>
          <StyledPanelTitle>STATUS</StyledPanelTitle>
          <hr />
          <StatusPanelContainer />
        </StyledPanelWrapper>
      </div>
    );
  }
}

export default connect(null, mapDispatchToProps)(App);
