import React, { Component } from 'react'
import NodePanel from '../components/NodePanel'

class NodePanelContainer extends Component {
  render() {
    return (
      <NodePanel />
    )
  }
}

export default NodePanelContainer
