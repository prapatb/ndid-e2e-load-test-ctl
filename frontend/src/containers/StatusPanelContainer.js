import React, { Component } from "react";
import { connect } from "react-redux";
import StatusPanel from "../components/StatusPanel";
import {   
  stopJob, 
  cancelJob, 
} from '../actions';

const mapStateToProps = state => {
  return {
    status: state.status
  };
};

const mapDispatchToProps = dispatch => {
  return {
    stopJob: jobId => dispatch(stopJob(jobId)),
    cancelJob: jobId => dispatch(cancelJob(jobId)),
  };
};

class StatusPanelContainer extends Component {
  handleGetAvgTimes = () => {};

  render() {
    return (
      <StatusPanel
        jobStatuses={this.props.status}
        onGetAvgTimes={this.handleGetAvgTimes}
        onConfirmStop={this.props.stopJob}
        onConfirmCancel={this.props.cancelJob}
      />
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StatusPanelContainer);
