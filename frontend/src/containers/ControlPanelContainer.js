import React, { Component } from 'react';
import { connect } from 'react-redux';
import ControlPanel from '../components/ControlPanel';
import {
  setJobIdText,
  setDurationText,
  setIntervalText,
  setRequestsPerIntervalText,
  setIdpListText,
  setAsListText,
  setRpAddressListText,
  setTimeoutText,
  startTest,
  stopTest
} from '../actions';
import {
  DEFAULT_NAMESPACE,
  DEFAULT_IDENTIFIER,
  DEFAULT_SERVICE_ID
} from '../config';

const mapStateToProps = state => {
  const {
    jobId,
    duration,
    interval,
    requestsPerInterval,
    idpList,
    asList,
    rpAddressList,
    timeout,
    isTesting
  } = state.control;

  return {
    jobId: jobId,
    duration: duration,
    interval: interval,
    requestsPerInterval: requestsPerInterval,
    idpList: idpList,
    asList: asList,
    rpAddressList: rpAddressList,
    timeout: timeout,
    isTesting: isTesting
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setJobIdText: value => dispatch(setJobIdText(value)),
    setDurationText: value => dispatch(setDurationText(value)),
    setIntervalText: value => dispatch(setIntervalText(value)),
    setRequestsPerIntervalText: value =>
      dispatch(setRequestsPerIntervalText(value)),
    setIdpListText: value => dispatch(setIdpListText(value)),
    setAsListText: value => dispatch(setAsListText(value)),
    setRpAddressListText: value => dispatch(setRpAddressListText(value)),
    setTimeoutText: value => dispatch(setTimeoutText(value)),
    startTest: (
      jobId,
      namespace,
      identifier,
      serviceId,
      duration,
      interval,
      requestsPerInterval,
      roundRobin,
      idpListText = '',
      asListText = '',
      rpAddressListText = '',
      timeout
    ) => {
      const idpList = idpListText.split(',').map(str => str.trim());
      const asList = asListText.split(',').map(str => str.trim());
      const rpAddressList = rpAddressListText
        .split(',')
        .map(str => str.trim())
        .map(str =>
          str.lastIndexOf('/') === str.length - 1
            ? str.substr(0, str.length - 1)
            : str
        );
      dispatch(
        startTest(
          jobId,
          namespace,
          identifier,
          serviceId,
          parseFloat(duration),
          parseFloat(interval),
          parseInt(requestsPerInterval),
          roundRobin,
          idpList,
          asList,
          rpAddressList,
          parseInt(timeout)
        )
      );
    },
    stopTest: () => dispatch(stopTest())
  };
};

class ControlPanelContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      namespace: DEFAULT_NAMESPACE,
      identifier: DEFAULT_IDENTIFIER,
      serviceId: DEFAULT_SERVICE_ID,
      roundRobin: true,
    };
  }

  handleJobIdChange = e => {
    this.props.setJobIdText(e.target.value);
  };

  handleNamespaceChange = e => {
    this.setState({ namespace: e.target.value });
  };

  handleIdentifierChange = e => {
    this.setState({ identifier: e.target.value });
  };

  handleServiceIdChange = e => {
    this.setState({ serviceId: e.target.value });
  };

  handleRoundRobinChange = e => {
    this.setState(prevState => ({ roundRobin: !prevState.roundRobin }));
  }

  handleDurationChange = e => {
    this.props.setDurationText(e.target.value);
  };

  handleIntervalChange = e => {
    this.props.setIntervalText(e.target.value);
  };

  handleRequestsPerIntervalChange = e => {
    this.props.setRequestsPerIntervalText(e.target.value);
  };

  handleIdpListChange = e => {
    this.props.setIdpListText(e.target.value);
  };

  handleAsListChange = e => {
    this.props.setAsListText(e.target.value);
  };

  handleRpAddressListChange = e => {
    this.props.setRpAddressListText(e.target.value);
  };

  handleTimeoutChange = e => {
    this.props.setTimeoutText(e.target.value);
  };

  handleStart = () => {
    this.props.startTest(
      this.props.jobId,
      this.state.namespace,
      this.state.identifier,
      this.state.serviceId,
      this.props.duration,
      this.props.interval,
      this.props.requestsPerInterval,
      this.state.roundRobin,
      this.props.idpList,
      this.props.asList,
      this.props.rpAddressList,
      this.props.timeout
    );
  };

  handleStop = () => {
    this.props.stopTest();
  };

  render() {
    const {
      jobId,
      duration,
      interval,
      requestsPerInterval,
      idpList,
      asList,
      rpAddressList,
      timeout,
      isTesting,
      setJobIdText,
      setDurationText,
      setIntervalText,
      startTest,
      stopTest,
      ...rest
    } = this.props;

    const { namespace, identifier, serviceId, roundRobin } = this.state;

    return (
      <ControlPanel
        jobId={jobId}
        namespace={namespace}
        identifier={identifier}
        serviceId={serviceId}
        duration={duration}
        interval={interval}
        requestsPerInterval={requestsPerInterval}
        roundRobin={roundRobin}
        idpList={idpList}
        asList={asList}
        rpAddressList={rpAddressList}
        timeout={timeout}
        isTesting={isTesting}
        onJobIdChange={this.handleJobIdChange}
        onNamespaceChange={this.handleNamespaceChange}
        onIdentifierChange={this.handleIdentifierChange}
        onServiceIdChange={this.handleServiceIdChange}
        onDurationChange={this.handleDurationChange}
        onIntervalChange={this.handleIntervalChange}
        onRequestsPerIntervalChange={this.handleRequestsPerIntervalChange}
        onRoundRobinChange={this.handleRoundRobinChange}
        onIdpListChange={this.handleIdpListChange}
        onAsListChange={this.handleAsListChange}
        onRpAddressListChange={this.handleRpAddressListChange}
        onTimeoutChange={this.handleTimeoutChange}
        onStart={this.handleStart}
        onStop={this.handleStop}
        {...rest}
      />
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ControlPanelContainer);
