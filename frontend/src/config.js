export const BACKEND_IP = process.env.BACKEND_IP || window.location.hostname;
export const BACKEND_PORT = process.env.BACKEND_PORT || window.location.port;
export const BACKEND_HTTPS = process.env.BACKEND_HTTPS === 'true' || false;
export const BACKEND_URL = `http${BACKEND_HTTPS ? 's' : ''}://${BACKEND_IP}:${BACKEND_PORT}`;

export const DEFAULT_NAMESPACE = 'confirm_1';
export const DEFAULT_IDENTIFIER = 'Test';
export const DEFAULT_SERVICE_ID = '001.cust_info_001';