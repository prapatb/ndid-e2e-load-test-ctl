import {
  SET_JOB_ID_TEXT,
  SET_DURATION_TEXT,
  SET_INTERVAL_TEXT,
  SET_REQUESTS_PER_INTERVAL_TEXT,
  SET_IDP_LIST_TEXT,
  SET_AS_LIST_TEXT,
  SET_IS_TESTING,
  SET_RP_ADDRESS_LIST_TEXT,
  SET_TIMEOUT_TEXT,
} from '../actions/actionTypes'

const initialState = {
  jobId: '',
  duration: '',
  interval: '',
  requestsPerInterval: '',
  idpList: '',
  asList: '',
  rpAddressList: '',
  timeout: '3600',
  isTesting: false,
  isRequesting: false,
};

const controlReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_JOB_ID_TEXT:
      return {
        ...state,
        jobId: action.value
      };
    case SET_DURATION_TEXT:
      return {
        ...state,
        duration: action.value
      };
    case SET_INTERVAL_TEXT:
      return {
        ...state,
        interval: action.value
      };
    case SET_REQUESTS_PER_INTERVAL_TEXT:
      return {
        ...state,
        requestsPerInterval: action.value
      };
    case SET_IDP_LIST_TEXT:
      return {
        ...state,
        idpList: action.value
      };
    case SET_AS_LIST_TEXT:
      return {
        ...state,
        asList: action.value
      };
    case SET_RP_ADDRESS_LIST_TEXT:
      return {
        ...state,
        rpAddressList: action.value
      };
    case SET_TIMEOUT_TEXT:
      return {
        ...state,
        timeout: action.value
      };
    case SET_IS_TESTING:
      return {
        ...state,
        isTesting: action.value === true
      };
  
    default:
      break;
  }
  return state;
};
export default controlReducer;
