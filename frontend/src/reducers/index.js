import { combineReducers } from 'redux'
import nodesReducer from './nodesReducer'
import controlReducer from './controlReducer'
import statusReducer from './statusReducer'

const rootReducer = combineReducers({ 
  nodes: nodesReducer,
  control: controlReducer,
  status: statusReducer
});

export default rootReducer;
