import {
  UPDATE_JOB_STATUS,
  START_TEST_SUCCESS,
  SET_ALL_JOBS,
  SET_IS_FETCHING_JOB_TIME_SUMMARY,
  FETCH_JOB_TIME_SUMMARY_SUCCESS,
  FETCH_JOB_TIME_SUMMARY_FAILED,
  SET_IS_STOPPING_JOB,
  SET_IS_CANCELLING_JOB,
} from '../actions/actionTypes';

const initialState = {};

const statusReducer = (state = initialState, action) => {
  switch (action.type) {
    case START_TEST_SUCCESS:
      return {
        ...state,
        [action.jobId]: {
          jobId: action.jobId
        }
      };
    case UPDATE_JOB_STATUS:
      return {
        ...state,
        [action.job.job_id]: action.job
      };
    case SET_ALL_JOBS:
      return action.jobs;
    case SET_IS_FETCHING_JOB_TIME_SUMMARY:
      return {
        ...state,
        [action.jobId]: {
          ...state[action.jobId],
          isFetchingTimeSummary: action.value
        }
      };
    case FETCH_JOB_TIME_SUMMARY_SUCCESS:
      return {
        ...state,
        [action.jobId]: {
          ...state[action.jobId],
          timeSummary: action.timeSummary
        }
      };
    case FETCH_JOB_TIME_SUMMARY_FAILED:
      return {
        ...state,
        [action.jobId]: {
          ...state[action.jobId],
          isFetchTimeSummaryFailed: true
        }
      };
    case SET_IS_STOPPING_JOB:
      return {
        ...state,
        [action.jobId]: {
          ...state[action.jobId],
          isStopping: action.value
        }
      };
    case SET_IS_CANCELLING_JOB:
      return {
        ...state,
        [action.jobId]: {
          ...state[action.jobId],
          isCancelling: action.value
        }
      };

    default:
      break;
  }
  return state;
};
export default statusReducer;
