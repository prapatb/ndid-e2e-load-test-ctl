import React from 'react'
import styled from 'styled-components'
import NodeList from './NodeList'

const StyledNodeType = styled.h3`
  font-weight: bold;
`

const NodePanel = props => (
  <div>
    {
      props.nodesByType && 
      Object
      .keys(props.nodesByType)
      .map(nodeType => (
        <div>
          <StyledNodeType>{nodeType}</StyledNodeType>
          <NodeList title={nodeType} nodes={props.nodesByType[nodeType]} />
        </div>
      ))
    }
  </div>
)

export default NodePanel  
