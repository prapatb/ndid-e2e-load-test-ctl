import React from 'react';
import JobStatus from './JobStatus';

const StatusPanel = props => (
  <div>
    {props.jobStatuses &&
      Object.keys(props.jobStatuses).map(jobId => (
        <JobStatus
          key={jobId}
          jobId={jobId}
          status={props.jobStatuses[jobId]}
          onConfirmStop={props.onConfirmStop}
          onConfirmCancel={props.onConfirmCancel}
        />
      ))}
  </div>
);

export default StatusPanel;
