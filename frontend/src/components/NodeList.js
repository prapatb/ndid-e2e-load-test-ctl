import React from 'react'
import styled from 'styled-components'
import Node from './Node'

const StyledDiv = styled.div`
  display: flex;
  flex-direction: row;
`

const NodeList = props => (
  <StyledDiv>
    {
      props.nodes && props.nodes.map(n => 
        <Node nodeName={n.nodeName} status={n.status} />
      )
    }
  </StyledDiv>
)

export default NodeList
