import React, { Component } from 'react';
import styled from 'styled-components';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

const StyledCheckboxFormGroup = styled(FormGroup)`
  margin-bottom: 1rem;
`

class ControlPanel extends Component {

  handleStart = (e) => {
    e.preventDefault();

    if (typeof this.props.onStart === 'function') {
      this.props.onStart();
    }    
  }

  handleStop = (e) => {
    e.preventDefault();

    if (typeof this.props.onStop === 'function') {
      this.props.onStop();
    }    
  }

  render() {  
    const { 
      jobId = '',
      namespace = '',
      identifier = '',
      duration = '', 
      interval = '', 
      requestsPerInterval = '',
      roundRobin = true,
      idpList = '',
      serviceId = '',
      asList = '',
      rpAddressList = '',
      timeout = '',
      onJobIdChange,
      onNamespaceChange,
      onIdentifierChange,
      onServiceIdChange,
      onDurationChange, 
      onIntervalChange,
      onRequestsPerIntervalChange,
      onRoundRobinChange,
      onIdpListChange,
      onAsListChange,
      onRpAddressListChange,
      onTimeoutChange,
      isTesting, 
      onStart, 
      onStop, 
      ...rest 
    } = this.props;

    return (
      <div {...rest} >
        <Form onSubmit={isTesting ? this.handleStop : this.handleStart}>
          <FormGroup>
            <Label for="job-id">Job ID (auto-generated if empty)</Label>
            <Input 
              type="textfield" 
              name="job-id" 
              id="job-id"
              disabled={isTesting}
              value={jobId}
              onChange={onJobIdChange}
            />
          </FormGroup>
          <FormGroup>
            <Label for="namespace">Namespace</Label>
            <Input 
              type="textfield" 
              name="namespace" 
              id="namespace"
              disabled={isTesting}
              value={namespace}
              onChange={onNamespaceChange}
            />
          </FormGroup>
          <FormGroup>
            <Label for="identifier">Identifier</Label>
            <Input 
              type="textfield" 
              name="identifier" 
              id="identifier"
              disabled={isTesting}
              value={identifier}
              onChange={onIdentifierChange}
            />
          </FormGroup>
          <FormGroup>
            <Label for="service-id">Service ID</Label>
            <Input 
              type="textfield" 
              name="service-id" 
              id="service-id"
              disabled={isTesting}
              value={serviceId}
              onChange={onServiceIdChange}
            />
          </FormGroup>
          <FormGroup>
            <Label for="duration">Duration</Label>
            <Input 
              type="textfield" 
              name="duration" 
              id="duration" 
              placeholder="in seconds" 
              disabled={isTesting}
              value={duration}
              onChange={onDurationChange}
            />
          </FormGroup>
          <FormGroup>
            <Label for="interval">Interval</Label>
            <Input 
              type="textfield" 
              name="interval" 
              id="interval" 
              placeholder="in seconds" 
              disabled={isTesting}
              value={interval}
              onChange={onIntervalChange}
            />
          </FormGroup>
          <FormGroup>
            <Label for="requests-per-interval">Requests per Interval</Label>
            <Input 
              type="textfield" 
              name="requests-per-interval" 
              id="requests-per-interval" 
              placeholder="e.g. 10" 
              disabled={isTesting}
              value={requestsPerInterval}
              onChange={onRequestsPerIntervalChange}
            />
          </FormGroup>
          <FormGroup>
            <Label for="idp-list">IdP List</Label>
            <Input 
              type="textfield" 
              name="idp-list" 
              id="idp-list" 
              placeholder="e.g. idp1,idp2" 
              disabled={isTesting}
              value={idpList}
              onChange={onIdpListChange}
            />
          </FormGroup>
          <FormGroup>
            <Label for="as-list">AS List</Label>
            <Input 
              type="textfield" 
              name="as-list" 
              id="as-list" 
              placeholder="e.g. as1,as2" 
              disabled={isTesting}
              value={asList}
              onChange={onAsListChange}
            />
          </FormGroup>
          <FormGroup>
            <Label for="rp-address-list">RP Address List</Label>
            <Input 
              type="textfield" 
              name="rp-address-list" 
              id="rp-address-list" 
              placeholder="e.g. http://api-rp-1:8080,http://api-rp-2:8080" 
              disabled={isTesting}
              value={rpAddressList}
              onChange={onRpAddressListChange}
            />
          </FormGroup>
          <FormGroup>
            <Label for="timeout">Timeout</Label>
            <Input 
              type="textfield" 
              name="timeout" 
              id="timeout" 
              placeholder="in seconds" 
              disabled={isTesting}
              value={timeout}
              onChange={onTimeoutChange}
            />
          </FormGroup>
          <StyledCheckboxFormGroup check>
            <Label check>
              <Input type="checkbox" checked={roundRobin} onChange={onRoundRobinChange}/>{' '}
              Round Robin
            </Label>
          </StyledCheckboxFormGroup>
          <Button color={isTesting ? 'danger' : 'primary'}>{isTesting ? 'Stop' : 'Start'}</Button>
        </Form>
      </div>
    );
  }
}

export default ControlPanel
