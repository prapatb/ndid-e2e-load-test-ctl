import React from 'react';
import styled from 'styled-components';
import _ from 'lodash';
import { Row, Col } from 'reactstrap';

const StyledRow = styled(Row)``;

const StyledJobConfigInfoItem = styled(Col)`
  text-align: center;
  margin-top: 0.7rem;
  margin-bottom: 1rem;

  display: flex;
  flex-direction: column;
  justify-content: flex-end;
`;

const StyledJobConfigInfoItemLabel = styled.div`
  font-size: 0.9rem;
  color: #444;
`;

const StyledJobConfigInfoItemValue = styled.div`
  margin-top: 0.2rem;
  font-weight: 500;
`;

const JobConfigInfo = ({ jobInfo }) => (
  <StyledRow>
    <StyledJobConfigInfoItem xs="6" md>
      <StyledJobConfigInfoItemLabel>Duration</StyledJobConfigInfoItemLabel>
      <StyledJobConfigInfoItemValue>
        {jobInfo.duration || '-'}
      </StyledJobConfigInfoItemValue>
    </StyledJobConfigInfoItem>
    <StyledJobConfigInfoItem xs="6" md>
      <StyledJobConfigInfoItemLabel>Interval</StyledJobConfigInfoItemLabel>
      <StyledJobConfigInfoItemValue>
        {jobInfo.interval || '-'}
      </StyledJobConfigInfoItemValue>
    </StyledJobConfigInfoItem>
    <StyledJobConfigInfoItem xs="6" md>
      <StyledJobConfigInfoItemLabel>
        Req(s) / Interval
      </StyledJobConfigInfoItemLabel>
      <StyledJobConfigInfoItemValue>
        {jobInfo.requestsPerInterval || '-'}
      </StyledJobConfigInfoItemValue>
    </StyledJobConfigInfoItem>
    <StyledJobConfigInfoItem xs="6" md>
      <StyledJobConfigInfoItemLabel>No. of IdP(s)</StyledJobConfigInfoItemLabel>
      <StyledJobConfigInfoItemValue>
        {jobInfo.idpList ? jobInfo.idpList.length : '-'}
      </StyledJobConfigInfoItemValue>
    </StyledJobConfigInfoItem>
    <StyledJobConfigInfoItem xs="6" md>
      <StyledJobConfigInfoItemLabel>No. of AS(es)</StyledJobConfigInfoItemLabel>
      <StyledJobConfigInfoItemValue>
        {jobInfo.asList ? jobInfo.asList.length : '-'}
      </StyledJobConfigInfoItemValue>
    </StyledJobConfigInfoItem>
    <StyledJobConfigInfoItem xs="6" md>
      <StyledJobConfigInfoItemLabel>No. of RP(s)</StyledJobConfigInfoItemLabel>
      <StyledJobConfigInfoItemValue>
        {jobInfo.rpAddressList ? jobInfo.rpAddressList.length : '-'}
      </StyledJobConfigInfoItemValue>
    </StyledJobConfigInfoItem>
    <StyledJobConfigInfoItem xs="6" md>
      <StyledJobConfigInfoItemLabel>Round Robin</StyledJobConfigInfoItemLabel>
      <StyledJobConfigInfoItemValue>
        {jobInfo.roundRobin != null ? (jobInfo.roundRobin ? 'Yes' : 'No') : '-'}
      </StyledJobConfigInfoItemValue>
    </StyledJobConfigInfoItem>
    <StyledJobConfigInfoItem xs="6" md>
      <StyledJobConfigInfoItemLabel>TPS</StyledJobConfigInfoItemLabel>
      <StyledJobConfigInfoItemValue>
        {(jobInfo.tps && _.round(jobInfo.tps, 2)) || '-'}
      </StyledJobConfigInfoItemValue>
    </StyledJobConfigInfoItem>
  </StyledRow>
);

export default JobConfigInfo;
