import React from 'react'
import styled from 'styled-components'

const ONLINE_COLOR = 'green';
const OFFLINE_COLOR = 'red';
const UNKNOWN_COLOR = 'grey';

const bgColor = status => {
  if (status === 'online') {
    return ONLINE_COLOR;
  }

  if (status === 'offline') {
    return OFFLINE_COLOR;
  }

  return UNKNOWN_COLOR;
}

const StyledBox = styled.div`
  width: 200px;
  height: 60px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${props => bgColor(props.status)};
  border: 1px solid #222;
`

const Node = props => (
  <StyledBox>
    { props.nodeName }
  </StyledBox>
)

export default Node
