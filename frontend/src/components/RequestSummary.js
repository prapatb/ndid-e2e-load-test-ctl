import React from 'react'
import styled from 'styled-components'

const StyledStatusEntry = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`

const RequestSummary = props => (
  <div>
    {
      props.data &&
      Object
      .keys(props.data)
      .map(status => (
        <StyledStatusEntry key={status}>
          <div>{status}</div>
          <div>{props.data[status]}</div>
        </StyledStatusEntry>
      ))
    }
  </div>
)

export default RequestSummary
