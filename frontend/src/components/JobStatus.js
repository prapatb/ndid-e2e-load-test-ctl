import React, { Component } from 'react';
import styled from 'styled-components';
import { Table, Button } from 'reactstrap';
import moment from 'moment';
import RequestSummary from './RequestSummary';
import JobConfigInfo from './JobConfigInfo';

const StyledButtonRow = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-bottom: 6px;
`;

const StyledNodeStatus = styled.div`
  margin-right: 10px;
  padding: 10px;
  border: 1px solid #222;
`;

const StyledJobId = styled.h3`
  font-weight: bold;
`;

const StyledJobStatus = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const StyledConfirmDiv = styled.div`
  button {
    margin-left: 4px;
  }
`;

const StyledTableWrapper = styled.div`
  overflow-x: auto;
`;

const StyledSummaryStatusValue = styled.td`
  font-weight: 600;
`

const StyledSummaryTimeHeader = styled.th`
  text-align: right;
`

const StyledSummaryTimeValue = styled.td`
  text-align: right;
`

const StyledTitleBar = styled.div`
  display: flex;
  justify-content: space-between;
`

const StyledCreatedAt = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  font-size: 0.7rem;
`

const StyledCreatedAtLabel = styled.div`
  color: #888;
`

const StyledCreatedAtValue = styled.div`
  font-weight: 500;
  color: #777;
`

class JobStatus extends Component {
  showStopConfirm = () => {
    this.setState({ shouldShowStopConfirm: true });
  };

  cancelStop = () => {
    this.setState({ shouldShowStopConfirm: false });
  };

  confirmStop = () => {
    if (typeof this.props.onConfirmStop === 'function') {
      this.props.onConfirmStop(this.props.jobId);
    }
  };

  showCancelConfirm = () => {
    this.setState({ shouldShowCancelConfirm: true });
  };

  cancelCancel = () => {
    this.setState({ shouldShowCancelConfirm: false });
  };

  confirmCancel = () => {
    if (typeof this.props.onConfirmCancel === 'function') {
      this.props.onConfirmCancel(this.props.jobId);
    }
  };

  constructor(props) {
    super(props);

    this.state = {
      shouldShowStopConfirm: false,
      shouldShowCancelConfirm: false,
    };
  }

  render() {
    const { jobId, status } = this.props;
    const { shouldShowStopConfirm, shouldShowCancelConfirm } = this.state;

    let stopBtnRowContent;
    if (status.isStopping) {
      stopBtnRowContent = 'Stopping...';
    } else if (shouldShowStopConfirm) {
      stopBtnRowContent = (
        <StyledConfirmDiv>
          Stop this job?
          <Button size="sm" onClick={this.cancelStop}>
            No, take me back
          </Button>
          <Button color="danger" size="sm" onClick={this.confirmStop}>
            Yes, stop job           
          </Button>
        </StyledConfirmDiv>
      );
    } else {
      stopBtnRowContent = (
        <Button color="danger" size="sm" onClick={this.showStopConfirm}>
          Stop Job
        </Button>
      );
    }

    let cancelBtnRowContent;
    if (status.isCancelling) {
      cancelBtnRowContent = 'Cancelling...';
    } else if (shouldShowCancelConfirm) {
      cancelBtnRowContent = (
        <StyledConfirmDiv>
          Cancel this job?
          <Button size="sm" onClick={this.cancelCancel}>
            No, take me back
          </Button>
          <Button color="danger" size="sm" onClick={this.confirmCancel}>
            Yes, cancel job
          </Button>
        </StyledConfirmDiv>
      );
    } else {
      cancelBtnRowContent = (
        <Button color="danger" size="sm" onClick={this.showCancelConfirm}>
          Cancel Job
        </Button>
      );
    }

    let stopStatus = status.stopped ? 'Yes' : 'No';
    if (status.forceStopped) {
      stopStatus += ' (Forced)';
    }

    let finishStatus = status.finished ? 'Yes' : 'No';
    if (status.cancelled) {
      finishStatus = 'Cancelled';
    }

    return (
      <StyledNodeStatus key={jobId}>
        <StyledTitleBar>
          <StyledJobId>{jobId}</StyledJobId>
          <StyledCreatedAt>
            <StyledCreatedAtLabel>Created At</StyledCreatedAtLabel>
            <StyledCreatedAtValue>{status.timestamps ? moment(status.timestamps.started).format('D-MM-YY HH:mm:ss') : '-'}</StyledCreatedAtValue>
          </StyledCreatedAt>
        </StyledTitleBar>
        <JobConfigInfo jobInfo={status} />
        {!status.stopped && (
          <StyledButtonRow>{stopBtnRowContent}</StyledButtonRow>
        )}
        {status.stopped && !status.finished && !status.cancelled && (
          <StyledButtonRow>{cancelBtnRowContent}</StyledButtonRow>
        )}
        <StyledJobStatus>
          <div>Stopped:</div>
          <div>{stopStatus}</div>
        </StyledJobStatus>
        <StyledJobStatus>
          <div>Finished:</div>
          <div>{finishStatus}</div>
        </StyledJobStatus>
        {status.cancelled && (
          <StyledJobStatus>
            <div></div>
            {status.forceClosedRequestCount >= status.toForceCloseRequestCount ?
              <div>All {status.toForceCloseRequestCount || 0} remaining request(s) closed</div> :
              <div>Closing remaining request(s) ... {status.forceClosedRequestCount || 0}/{status.toForceCloseRequestCount || 0}</div>
            }
          </StyledJobStatus>
        )}
        <hr />
        <RequestSummary data={status.summary} />
        {status.finished && status.timeSummary && (
          <React.Fragment>
            <hr />
            <StyledTableWrapper>
              <Table striped>
                <thead>
                  <tr>
                    <th>Status</th>
                    <StyledSummaryTimeHeader>Min</StyledSummaryTimeHeader>
                    <StyledSummaryTimeHeader>Avg</StyledSummaryTimeHeader>
                    <StyledSummaryTimeHeader>Max</StyledSummaryTimeHeader>
                  </tr>
                </thead>
                <tbody>
                  {status.timeSummary.state && Object.keys(status.timeSummary.state).map(reqState => (
                    <tr key={reqState}>
                      <StyledSummaryStatusValue scope="row">{reqState}</StyledSummaryStatusValue>
                      <StyledSummaryTimeValue>{status.timeSummary.state[reqState].min.toFixed(2)}</StyledSummaryTimeValue>
                      <StyledSummaryTimeValue>{status.timeSummary.state[reqState].avg.toFixed(2)}</StyledSummaryTimeValue>
                      <StyledSummaryTimeValue>{status.timeSummary.state[reqState].max.toFixed(2)}</StyledSummaryTimeValue>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </StyledTableWrapper>
          </React.Fragment>
        )}
      </StyledNodeStatus>
    );
  }
}

export default JobStatus;
