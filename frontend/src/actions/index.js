import {
  SET_RP_ADDRESS_LIST_TEXT,
  SET_JOB_ID_TEXT,
  SET_DURATION_TEXT,
  SET_INTERVAL_TEXT,
  SET_REQUESTS_PER_INTERVAL_TEXT,
  SET_IDP_LIST_TEXT,
  SET_AS_LIST_TEXT,
  SET_IS_TESTING,
  SET_IS_REQUESTING,
  START_TEST,
  START_TEST_SUCCESS,
  START_TEST_FAILED,
  STOP_TEST,
  STOP_TEST_SUCCESS,
  STOP_TEST_FAILED,
  UPDATE_JOB_STATUS,
  FETCH_ALL_JOBS,
  SET_ALL_JOBS,
  SET_IS_FETCHING_ALL_JOBS,
  APP_INITIALIZED,
  FETCH_JOB_TIME_SUMMARY,
  SET_IS_FETCHING_JOB_TIME_SUMMARY,
  FETCH_JOB_TIME_SUMMARY_SUCCESS,
  FETCH_JOB_TIME_SUMMARY_FAILED,
  SET_TIMEOUT_TEXT,
  STOP_JOB,
  STOP_JOB_SUCCESS,
  STOP_JOB_FAILED,
  SET_IS_STOPPING_JOB,
  CANCEL_JOB,
  CANCEL_JOB_SUCCESS,
  CANCEL_JOB_FAILED,
  SET_IS_CANCELLING_JOB,
} from './actionTypes';

export const initialized = () => ({ type: APP_INITIALIZED });
export const setRpAddressListText = value => ({
  type: SET_RP_ADDRESS_LIST_TEXT,
  value: value
});
export const setJobIdText = value => ({ type: SET_JOB_ID_TEXT, value: value });
export const setDurationText = value => ({
  type: SET_DURATION_TEXT,
  value: value
});
export const setIntervalText = value => ({
  type: SET_INTERVAL_TEXT,
  value: value
});
export const setRequestsPerIntervalText = value => ({
  type: SET_REQUESTS_PER_INTERVAL_TEXT,
  value: value
});
export const setIdpListText = value => ({
  type: SET_IDP_LIST_TEXT,
  value: value
});
export const setAsListText = value => ({
  type: SET_AS_LIST_TEXT,
  value: value
});
export const setTimeoutText = value => ({
  type: SET_TIMEOUT_TEXT,
  value: value
});
export const setIsTesting = value => ({ type: SET_IS_TESTING, value: value });
export const setIsRequesting = value => ({
  type: SET_IS_REQUESTING,
  value: value
});
export const startTest = (
  jobId,
  namespace,
  identifier,
  serviceId,
  duration,
  interval,
  requestsPerInterval,
  idpList,
  asList,
  rpAddressList,
  timeout
) => ({
  type: START_TEST,
  jobId,
  namespace,
  identifier,
  serviceId,
  duration,
  interval,
  requestsPerInterval,
  idpList,
  asList,
  rpAddressList,
  timeout
});
export const startTestSuccess = jobId => ({
  type: START_TEST_SUCCESS,
  jobId: jobId
});
export const startTestFailed = () => ({ type: START_TEST_FAILED });
export const stopTest = () => ({ type: STOP_TEST });
export const stopTestSuccess = () => ({ type: STOP_TEST_SUCCESS });
export const stopTestFailed = () => ({ type: STOP_TEST_FAILED });
export const updateJobStatus = job => ({ type: UPDATE_JOB_STATUS, job: job });
export const fetchAllJobs = () => ({ type: FETCH_ALL_JOBS });
export const setAllJobs = jobs => ({ type: SET_ALL_JOBS, jobs: jobs });
export const setIsFetchingAllJobs = value => ({
  type: SET_IS_FETCHING_ALL_JOBS,
  value: value
});
export const fetchJobTimeSummary = jobId => ({
  type: FETCH_JOB_TIME_SUMMARY,
  jobId: jobId
});
export const setIsFetchingJobTimeSummary = (jobId, value) => ({
  type: SET_IS_FETCHING_JOB_TIME_SUMMARY,
  jobId: jobId,
  value: value
});
export const fetchJobTimeSummarySuccess = (jobId, timeSummary) => ({
  type: FETCH_JOB_TIME_SUMMARY_SUCCESS,
  jobId: jobId,
  timeSummary: timeSummary
});
export const fetchJobTimeSummaryFailed = jobId => ({
  type: FETCH_JOB_TIME_SUMMARY_FAILED,
  jobId: jobId
});
export const stopJob = jobId => ({ type: STOP_JOB, jobId: jobId });
export const stopJobSuccess = jobId => ({
  type: STOP_JOB_SUCCESS,
  jobId: jobId
});
export const stopJobFailed = jobId => ({ type: STOP_JOB_FAILED, jobId: jobId });
export const setIsStoppingJob = (jobId, value) => ({
  type: SET_IS_STOPPING_JOB,
  jobId: jobId,
  value
});
export const cancelJob = jobId => ({ type: CANCEL_JOB, jobId: jobId });
export const cancelJobSuccess = jobId => ({
  type: CANCEL_JOB_SUCCESS,
  jobId: jobId
});
export const cancelJobFailed = jobId => ({ type: CANCEL_JOB_FAILED, jobId: jobId });
export const setIsCancellingJob = (jobId, value) => ({
  type: SET_IS_CANCELLING_JOB,
  jobId: jobId,
  value
});
