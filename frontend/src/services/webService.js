import axios from 'axios'

export const request = (url, method = 'get', body, config) => {
  const axiosConfig = { 
    ...config, 
    url: url, 
    method: method.toLowerCase()
  };

  if (axiosConfig.method !== 'get') {
    axiosConfig.data = body;
  }

  return axios(axiosConfig);
}

export const get = (url, config) => {
  return request(url, 'GET');
}

export const post = (url, body, config) => {
  return request(url, 'POST', body);
}
