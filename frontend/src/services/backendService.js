import { get, post } from './webService';
import { BACKEND_URL } from '../config';

export const startTest = (
  jobId,
  namespace,
  identifier,
  serviceId,
  duration,
  interval,
  requestsPerInterval,
  roundRobin,
  idpList,
  asList,
  rpAddressList,
  timeout
) => {
  return post(BACKEND_URL + '/api/createRequests', {
    jobId: jobId,
    namespace,
    identifier,
    serviceId,
    duration,
    interval,
    requestsPerInterval,
    roundRobin,
    idpList,
    asList,
    rpAddressList,
    timeout
  });
};

export const stopTest = jobId => {
  return post(BACKEND_URL + '/api/stopJob/' + jobId);
};

export const fetchAllJobs = () => {
  return get(BACKEND_URL + '/api/jobs');
};

export const fetchJobTimeSummary = jobId => {
  return get(BACKEND_URL + '/api/jobTimeSummary/' + jobId);
};

export const stopJob = jobId => {
  return post(BACKEND_URL + '/api/stopJob/' + jobId);
};

export const cancelJob = jobId => {
  return post(BACKEND_URL + '/api/cancelJob/' + jobId);
};
