import { take, takeEvery, all, put, call, fork } from "redux-saga/effects";
import { eventChannel } from "redux-saga";
import io from "socket.io-client";
import {
  START_TEST,
  STOP_TEST,
  APP_INITIALIZED,
  FETCH_ALL_JOBS,
  FETCH_JOB_TIME_SUMMARY,
  STOP_JOB,
  CANCEL_JOB,
} from "../actions/actionTypes";
import {
  setIsRequesting,
  startTestSuccess,
  updateJobStatus,
  startTestFailed,
  setIsFetchingAllJobs,
  setAllJobs,
  setIsFetchingJobTimeSummary,
  fetchJobTimeSummarySuccess,
  fetchJobTimeSummaryFailed,
  stopJobSuccess,
  stopJobFailed,
  setIsStoppingJob,
  cancelJobSuccess,
  cancelJobFailed,
  setIsCancellingJob,
} from "../actions";
import * as actionCreator from "../actions";
import * as backendService from "../services/backendService";
import { BACKEND_URL } from "../config";

const connect = url => {
  console.log("Open socket to " + url);
  const socket = io(url, { path: "/ws" });
  return new Promise(resolve => {
    socket.on("connect", () => {
      resolve(socket);
    });
  });
};

function* initialize() {
  yield fork(fetchAllJobs);

  // Create socket
  const socket = yield call(connect, BACKEND_URL);
  yield fork(createChannel, socket);
}

function* createChannel(socket) {
  const channel = yield call(subscribe, socket);
  while (true) {
    let action = yield take(channel);
    yield put(action);
  }
}

export function* watchAll() {
  yield all([
    takeEvery(APP_INITIALIZED, initialize),
    takeEvery(START_TEST, startTest),
    takeEvery(STOP_TEST, stopTest),
    takeEvery(FETCH_ALL_JOBS, fetchAllJobs),
    takeEvery(FETCH_JOB_TIME_SUMMARY, fetchJobTimeSummary),
    takeEvery(STOP_JOB, stopJob),
    takeEvery(CANCEL_JOB, cancelJob),
  ]);
}

export function* subscribe(socket) {
  return new eventChannel(emit => {
    socket.on("job:update", job => {
      console.log("Received job:update: ", job);
      emit(updateJobStatus(job));
      if (job.finished) {
        emit(actionCreator.fetchJobTimeSummary(job.job_id));
      }
    });
    return () => {
      // This is a handler to unsubscribe.
    };
  });
}

function* startTest(action) {
  yield put(setIsRequesting(true));

  try {
    const response = yield call(
      backendService.startTest,
      action.jobId,
      action.namespace,
      action.identifier,
      action.serviceId,
      action.duration,
      action.interval,
      action.requestsPerInterval,
      action.idpList,
      action.asList,
      action.rpAddressList,
      action.timeout
    );
    yield put(startTestSuccess(response.data.job_id));
  } catch (error) {
    yield put(startTestFailed("Start failed"));
  }

  yield put(setIsRequesting(false));
}

function* stopTest(action) {
  yield put(setIsRequesting(true));

  yield call(backendService.stopTest, action.jobId);

  yield put(setIsRequesting(false));
}

function* fetchAllJobs() {
  yield put(setIsFetchingAllJobs(true));

  try {
    const result = yield call(backendService.fetchAllJobs);
    yield put(setAllJobs(result.data));
  } catch (error) {
    console.error("Fetching all jobs failed", error);
  }

  yield put(setIsFetchingAllJobs(false));
}

function* fetchJobTimeSummary(action) {
  yield put(setIsFetchingJobTimeSummary(action.jobId, true));

  try {
    const result = yield call(backendService.fetchJobTimeSummary, action.jobId);
    yield put(fetchJobTimeSummarySuccess(action.jobId, result.data));
  } catch (error) {
    console.error(
      "Fetching time summary of jobId (" + action.jobId + ") failed",
      error
    );
    yield put(fetchJobTimeSummaryFailed(action.jobId));
  }

  yield put(setIsFetchingJobTimeSummary(action.jobId, false));
}

function* stopJob(action) {
  yield put(setIsStoppingJob(action.jobId, true));

  try {
    yield call(backendService.stopJob, action.jobId);
    yield put(stopJobSuccess(action.jobId));

    // NOTE:  We won't set is stopping job to false when succeed
    //        Due to stopped status in UI is updated intervally (1 sec)
    //        which could make the UI to be in inconsistent state
    //        (See stop button's conditional rendering logic for more details)
  } catch (error) {
    console.error(
      "Stoping job of jobId (" + action.jobId + ") failed",
      error
    );
    yield put(stopJobFailed(action.jobId));
    yield put(setIsStoppingJob(action.jobId, false));
  }
}

function* cancelJob(action) {
  yield put(setIsCancellingJob(action.jobId, true));

  try {
    yield call(backendService.cancelJob, action.jobId);
    yield put(cancelJobSuccess(action.jobId));

    // NOTE:  We won't set is cancelling job to false when succeed
    //        Due to cancelled status in UI is updated intervally (1 sec)
    //        which could make the UI to be in inconsistent state
    //        (See cancel button's conditional rendering logic for more details)
  } catch (error) {
    console.error(
      "Cancelling job of jobId (" + action.jobId + ") failed",
      error
    );
    yield put(cancelJobFailed(action.jobId));
    yield put(setIsCancellingJob(action.jobId, false));
  }
}
