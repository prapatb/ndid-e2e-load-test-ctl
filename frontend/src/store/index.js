import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "../reducers";
import createSagaMiddleware from "redux-saga";
import { watchAll } from "../sagas";

const reduxDevTools =
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  rootReducer, 
  // compose(applyMiddleware(sagaMiddleware), reduxDevTools),
  applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(watchAll);

export default store;
